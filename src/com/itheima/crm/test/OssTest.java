package com.itheima.crm.test;

import java.io.File;
import java.net.URL;
import java.util.Date;

import org.junit.Test;

import com.aliyun.oss.OSSClient;
import com.aliyun.oss.model.PutObjectResult;

public class OssTest {

	@Test
	public void upload() {
		// endpoint以杭州为例，其它region请按实际情况填写
		String endpoint = "http://oss-cn-beijing.aliyuncs.com";
		// 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建
		String accessKeyId = "LTAI5t1TiVl1ZWN0";
		String accessKeySecret = "oTBU8yJnnyWYRKSfAwEclsSnP2v7Zl";
		// 创建OSSClient实例
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
		// 上传文件
		PutObjectResult pr = ossClient.putObject("fzh", "timg.jpg", new File("C:\\Users\\fengzihao\\Desktop\\timg.jpg"));
		// 关闭client
		ossClient.shutdown();
	}
	
	@Test
	public void getUrl() {
		// endpoint以杭州为例，其它region请按实际情况填写
		String endpoint = "http://oss-cn-beijing.aliyuncs.com";
		// 云账号AccessKey有所有API访问权限，建议遵循阿里云安全最佳实践，创建并使用RAM子账号进行API访问或日常运维，请登录 https://ram.console.aliyun.com 创建
		String accessKeyId = "LTAI5t1TiVl1ZWN0";
		String accessKeySecret = "oTBU8yJnnyWYRKSfAwEclsSnP2v7Zl";
		String bucketName = "fzh";
		String key = "timg.jpg";
		// 创建OSSClient实例
		OSSClient ossClient = new OSSClient(endpoint, accessKeyId, accessKeySecret);
		// 设置URL过期时间为1小时
		Date expiration = new Date(new Date().getTime() + 3600 * 1000);
		// 生成URL
		URL url = ossClient.generatePresignedUrl(bucketName, key, expiration);
		System.out.println("url="+url);
	}
}
