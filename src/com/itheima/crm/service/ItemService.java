package com.itheima.crm.service;

import java.util.List;

import com.itheima.crm.pojo.TbItem;

public interface ItemService {
	//根据Id查询信息
	public TbItem getById(long id)throws Exception;
	
	public List<TbItem> getByName(long title) throws Exception;
}
