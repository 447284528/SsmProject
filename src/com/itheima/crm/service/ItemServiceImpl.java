package com.itheima.crm.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.itheima.crm.mapper.TbItemMapper;
import com.itheima.crm.pojo.TbItem;

@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	private TbItemMapper tbItemMapper;
	
	@Override
	public TbItem getById(long id) throws Exception {
		// TODO Auto-generated method stub
		return tbItemMapper.selectByPrimaryKey(id);
	}
	
	public List<TbItem> getByName(long id) throws Exception {
		return tbItemMapper.selectByTitle(id);
	}


}
